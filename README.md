# Electricity Consumption Presenter

## Description

I'm sharing my recruitment assignment. 

Solution is build with spring framework.
Code is tested with integration and unit tests.
For frontend I used Bootstrap and JQuery.
In this case frontend was really simple, so I created them with the simplest way
and I've focused on backend.

If something is missing or if you just want me to add something just contact me.

## Installing

Just install with Maven and run ElectricityConsumptionApplication. 
Url with the default ports is http://localhost:8080/