package pl.pmierkowski.electricityconsumption.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.pmierkowski.electricityconsumption.exception.InvalidDateRangeException;
import pl.pmierkowski.electricityconsumption.model.GroupByField;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@RestClientTest(ElectricityConsumptionService.class)
public class ElectricityConsumptionServiceTest {

    @Autowired
    private ElectricityConsumptionService electricityConsumptionService;

    @Test(expected = InvalidDateRangeException.class)
    public void shouldReturnExceptionWhenSetInvalidDate() {
        this.electricityConsumptionService.getGroupedConsumption(LocalDate.now(), LocalDate.now(), GroupByField.fromString("day"));
    }
}
