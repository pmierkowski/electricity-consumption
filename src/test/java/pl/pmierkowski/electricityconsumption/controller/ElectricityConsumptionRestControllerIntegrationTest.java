package pl.pmierkowski.electricityconsumption.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ElectricityConsumptionRestController.class)
public class ElectricityConsumptionRestControllerIntegrationTest {
    @Autowired

    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void shouldReturnEnergyReportWhenGivenValidQuery() throws Exception {
        this.mockMvc.perform(
                get(
                        "/energy-reports?start={start}&end={end}&group_by={group_by}",
                        LocalDate.now().minusDays(2).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        "day"
                )
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.[0].date").value(LocalDate.now().minusDays(2).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))))
                .andDo(print());
    }

    @Test
    public void shouldReturnErrorMessageWhenThereAreNoInputParameters() throws Exception {
        this.mockMvc.perform(
                get("/energy-reports")
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("Required parameter 'start' is missing"))
                .andDo(print());
    }

    @Test
    public void shouldReturnErrorMessageWhenDataRangesAreInvalid() throws Exception {
        String startNeverFromEnd = LocalDate.now().plusDays(5).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        this.mockMvc.perform(
                get(
                        "/energy-reports?start={start}&end={end}&group_by={group_by}",
                        startNeverFromEnd,
                        LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        "day"
                )
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("Start date must be before end date"))
                .andDo(print());


        String endFromTheFuture = LocalDate.now().plusDays(10).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        this.mockMvc.perform(
                get(
                        "/energy-reports?start={start}&end={end}&group_by={group_by}",
                        LocalDate.now().minusDays(2).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        endFromTheFuture,
                        "day"
                )
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("End date must be before now"))
                .andDo(print());
    }

    @Test
    public void shouldReturnErrorMessageWhereToLongDateRangeIsInput() throws Exception {
        this.mockMvc.perform(
                get(
                        "/energy-reports?start={start}&end={end}&group_by={group_by}",
                        LocalDate.now().minusYears(5).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        "day"
                )
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("Maximum date range is 2 years"))
                .andDo(print());
    }

    @Test
    public void shouldReturnErrorMessageWhenGroupByIsEmpty() throws Exception {
        this.mockMvc.perform(
                get(
                        "/energy-reports?start={start}&end={end}",
                        LocalDate.now().minusDays(2).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
                )
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value("Required parameter 'group_by' is missing"))
                .andDo(print());
    }

    @Test
    public void shouldReturnErrorMessageWhenGroupByIsIncorrect() throws Exception {
        String invalidGroupByField = "WrongGroupByFieldValue";

        this.mockMvc.perform(
                get(
                        "/energy-reports?start={start}&end={end}&group_by={group_by}",
                        LocalDate.now().minusDays(2).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                        invalidGroupByField
                )
        )
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.message").value(String.format("Group: %s is invalid", invalidGroupByField)))
                .andDo(print());
    }
}
