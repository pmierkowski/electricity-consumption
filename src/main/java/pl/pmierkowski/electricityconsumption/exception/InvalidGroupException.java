package pl.pmierkowski.electricityconsumption.exception;

public class InvalidGroupException extends RuntimeException {
    public InvalidGroupException(String group) {
        super(String.format("Group: %s is invalid", group));
    }
}
