package pl.pmierkowski.electricityconsumption.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pmierkowski.electricityconsumption.model.Consumption;
import pl.pmierkowski.electricityconsumption.model.ElectricityConsumptionRepository;
import pl.pmierkowski.electricityconsumption.model.GroupByField;
import pl.pmierkowski.electricityconsumption.validator.DateRangeValidator;

import java.time.LocalDate;
import java.util.List;

@Service
public class ElectricityConsumptionService {

    private final ElectricityConsumptionRepository electricityConsumptionRepository;

    @Autowired
    public ElectricityConsumptionService(ElectricityConsumptionRepository electricityConsumptionRepository) {
        this.electricityConsumptionRepository = electricityConsumptionRepository;
    }

    public List<Consumption> getGroupedConsumption(LocalDate start, LocalDate end, GroupByField groupByField) {
        DateRangeValidator.validate(start, end);

        return this.electricityConsumptionRepository.getGroupedConsumption(start, end, groupByField);
    }
}
