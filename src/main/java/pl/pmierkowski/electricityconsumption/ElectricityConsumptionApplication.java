package pl.pmierkowski.electricityconsumption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "pl.pmierkowski.electricityconsumption")
public class ElectricityConsumptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElectricityConsumptionApplication.class, args);
    }
}
