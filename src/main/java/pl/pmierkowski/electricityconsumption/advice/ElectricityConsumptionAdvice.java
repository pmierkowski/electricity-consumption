package pl.pmierkowski.electricityconsumption.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.pmierkowski.electricityconsumption.exception.InvalidDateRangeException;
import pl.pmierkowski.electricityconsumption.exception.InvalidGroupException;
import pl.pmierkowski.electricityconsumption.model.ErrorResponse;

@ControllerAdvice
public class ElectricityConsumptionAdvice {

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorResponse> handleMissingServletRequestParameterException(MissingServletRequestParameterException exception) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.BAD_REQUEST.value(),
                        String.format("Required parameter '%s' is missing", exception.getParameterName()),
                        String.format("You need to provide '%s' parameter '%s'", exception.getParameterType(), exception.getParameterName())
                ),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler({InvalidGroupException.class})
    public ResponseEntity<ErrorResponse> handleInvalidGroupException(InvalidGroupException exception) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.BAD_REQUEST.value(),
                        exception.getMessage(),
                        "Check your input fields"
                ),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler({InvalidDateRangeException.class})
    public ResponseEntity<ErrorResponse> handleInvalidDateRangeException(InvalidDateRangeException exception) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.BAD_REQUEST.value(),
                        exception.getMessage(),
                        "Check your date input fields"
                ),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception exception) {
        return new ResponseEntity<>(
                new ErrorResponse(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(),
                        exception.getMessage(),
                        "Internal server error, try again later."
                ),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
}
