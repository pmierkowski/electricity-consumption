package pl.pmierkowski.electricityconsumption.validator;

import pl.pmierkowski.electricityconsumption.exception.InvalidDateRangeException;

import java.time.LocalDate;

public class DateRangeValidator {
    private static final int YEARS_OFFSET = 2;

    public static void validate(LocalDate start, LocalDate end) {
        if(!start.equals(end) && !start.isBefore(end)){
            throw new InvalidDateRangeException("Start date must be before end date");
        }
        if(!end.isBefore(LocalDate.now())){
            throw new InvalidDateRangeException("End date must be before now");
        }
        if(!start.isAfter(LocalDate.now().minusYears(YEARS_OFFSET))){
            throw new InvalidDateRangeException(String.format("Maximum date range is %s years", YEARS_OFFSET));
        }
    }
}
