package pl.pmierkowski.electricityconsumption.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.pmierkowski.electricityconsumption.configuration.CacheConfiguration;
import pl.pmierkowski.electricityconsumption.model.Consumption;
import pl.pmierkowski.electricityconsumption.model.ElectricityConsumptionRepository;
import pl.pmierkowski.electricityconsumption.model.GroupByField;
import pl.pmierkowski.electricityconsumption.model.energyreport.EnergyReportType;
import pl.pmierkowski.electricityconsumption.model.energyreport.HourConsumptionType;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ElectricityConsumptionApiRepository implements ElectricityConsumptionRepository {

    private final static int DAYS_OFFSET = 1;

    private final RestTemplate restTemplate;
    private final UriComponentsBuilder uriComponentsBuilder;

    @Autowired
    public ElectricityConsumptionApiRepository(RestTemplate restTemplate, @Qualifier("finestmediaUriComponentsBuilder") UriComponentsBuilder uriComponentsBuilder) {
        this.restTemplate = restTemplate;
        this.uriComponentsBuilder = uriComponentsBuilder;
    }

    @Override
    @Cacheable(CacheConfiguration.ENERGY_CONSUMPTION)
    public List<Consumption> getGroupedConsumption(LocalDate start, LocalDate end, GroupByField groupByField) {

        URI uri = this.uriComponentsBuilder
                .replaceQueryParam("start", start)
                .replaceQueryParam("end", end)
                .build()
                .toUri();

        EnergyReportType energyReportType = this.restTemplate.getForObject(uri, EnergyReportType.class);

        List<Consumption> consumption = new ArrayList<>();

        if (null != energyReportType) {
            this.sumConsumption(energyReportType, start, end, groupByField)
                    .forEach((date, usage) -> consumption.add(new Consumption(usage, date)));

            Collections.sort(consumption);
        }

        return consumption;
    }

    private boolean isConsumptionDateInValidDataRanges(LocalDate now, LocalDate start, LocalDate end) {
        return now.isAfter(start.minusDays(DAYS_OFFSET)) && now.isBefore(end.plusDays(DAYS_OFFSET));

    }

    private Map<String, Double> sumConsumption(EnergyReportType energyReportType, LocalDate start, LocalDate end, GroupByField groupByField) {
        return energyReportType
                .getAccountTimeSeries()
                .getConsumptionHistory()
                .getHourConsumption()
                .stream()
                .filter(hourConsumptionType -> this.isConsumptionDateInValidDataRanges(hourConsumptionType.getTs().toGregorianCalendar().toZonedDateTime().toLocalDate(), start, end))
                .collect(
                        Collectors.groupingBy(
                                hourConsumptionType -> hourConsumptionType.getTs().toGregorianCalendar().toZonedDateTime().toLocalDate().format(DateTimeFormatter.ofPattern(groupByField.getValue())),
                                Collectors.summingDouble(HourConsumptionType::getValue)
                        )
                );
    }
}
