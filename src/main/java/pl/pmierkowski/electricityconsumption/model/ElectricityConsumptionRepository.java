package pl.pmierkowski.electricityconsumption.model;

import java.time.LocalDate;
import java.util.List;

public interface ElectricityConsumptionRepository {

    List<Consumption> getGroupedConsumption(LocalDate start, LocalDate end, GroupByField groupByField);
}
