package pl.pmierkowski.electricityconsumption.model;

import java.util.Objects;

public class Consumption implements Comparable {
    private double usage;
    private String date;

    public Consumption(double usage, String date) {
        this.usage = usage;
        this.date = date;
    }

    public double getUsage() {
        return usage;
    }

    public String getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consumption that = (Consumption) o;
        return Double.compare(that.usage, usage) == 0 &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usage, date);
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Consumption) {
            Consumption consumption = (Consumption) o;
            return -1 * consumption.getDate().compareTo(this.getDate());
        }

        return -1;
    }
}
