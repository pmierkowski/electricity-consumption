
package pl.pmierkowski.electricityconsumption.model.energyreport;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountTimeSeriesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountTimeSeriesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MeasurementUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountingPoint" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConsumptionHistory" type="{}ConsumptionHistoryType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountTimeSeriesType", propOrder = {
    "measurementUnit",
    "accountingPoint",
    "consumptionHistory"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class AccountTimeSeriesType {

    @XmlElement(name = "MeasurementUnit", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String measurementUnit;
    @XmlElement(name = "AccountingPoint", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String accountingPoint;
    @XmlElement(name = "ConsumptionHistory", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected ConsumptionHistoryType consumptionHistory;

    /**
     * Gets the value of the measurementUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getMeasurementUnit() {
        return measurementUnit;
    }

    /**
     * Sets the value of the measurementUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setMeasurementUnit(String value) {
        this.measurementUnit = value;
    }

    /**
     * Gets the value of the accountingPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getAccountingPoint() {
        return accountingPoint;
    }

    /**
     * Sets the value of the accountingPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAccountingPoint(String value) {
        this.accountingPoint = value;
    }

    /**
     * Gets the value of the consumptionHistory property.
     * 
     * @return
     *     possible object is
     *     {@link ConsumptionHistoryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public ConsumptionHistoryType getConsumptionHistory() {
        return consumptionHistory;
    }

    /**
     * Sets the value of the consumptionHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsumptionHistoryType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setConsumptionHistory(ConsumptionHistoryType value) {
        this.consumptionHistory = value;
    }

}
