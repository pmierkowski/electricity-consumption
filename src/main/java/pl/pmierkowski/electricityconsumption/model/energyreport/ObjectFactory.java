
package pl.pmierkowski.electricityconsumption.model.energyreport;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pl.pmierkowski.electricityconsumption.model.energyreport package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EnergyReport_QNAME = new QName("", "EnergyReport");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pl.pmierkowski.electricityconsumption.model.energyreport
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EnergyReportType }
     * 
     */
    public EnergyReportType createEnergyReportType() {
        return new EnergyReportType();
    }

    /**
     * Create an instance of {@link AccountTimeSeriesType }
     * 
     */
    public AccountTimeSeriesType createAccountTimeSeriesType() {
        return new AccountTimeSeriesType();
    }

    /**
     * Create an instance of {@link HourConsumptionType }
     * 
     */
    public HourConsumptionType createHourConsumptionType() {
        return new HourConsumptionType();
    }

    /**
     * Create an instance of {@link ConsumptionHistoryType }
     * 
     */
    public ConsumptionHistoryType createConsumptionHistoryType() {
        return new ConsumptionHistoryType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnergyReportType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EnergyReport")
    public JAXBElement<EnergyReportType> createEnergyReport(EnergyReportType value) {
        return new JAXBElement<EnergyReportType>(_EnergyReport_QNAME, EnergyReportType.class, null, value);
    }

}
