
package pl.pmierkowski.electricityconsumption.model.energyreport;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EnergyReportType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnergyReportType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocumentIdentification" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DocumentDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="AccountTimeSeries" type="{}AccountTimeSeriesType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnergyReportType", propOrder = {
    "documentIdentification",
    "documentDateTime",
    "accountTimeSeries"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class EnergyReportType {

    @XmlElement(name = "DocumentIdentification")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected int documentIdentification;
    @XmlElement(name = "DocumentDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected XMLGregorianCalendar documentDateTime;
    @XmlElement(name = "AccountTimeSeries", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected AccountTimeSeriesType accountTimeSeries;

    /**
     * Gets the value of the documentIdentification property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public int getDocumentIdentification() {
        return documentIdentification;
    }

    /**
     * Sets the value of the documentIdentification property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDocumentIdentification(int value) {
        this.documentIdentification = value;
    }

    /**
     * Gets the value of the documentDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public XMLGregorianCalendar getDocumentDateTime() {
        return documentDateTime;
    }

    /**
     * Sets the value of the documentDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setDocumentDateTime(XMLGregorianCalendar value) {
        this.documentDateTime = value;
    }

    /**
     * Gets the value of the accountTimeSeries property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTimeSeriesType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public AccountTimeSeriesType getAccountTimeSeries() {
        return accountTimeSeries;
    }

    /**
     * Sets the value of the accountTimeSeries property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTimeSeriesType }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setAccountTimeSeries(AccountTimeSeriesType value) {
        this.accountTimeSeries = value;
    }

}
