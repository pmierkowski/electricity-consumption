
package pl.pmierkowski.electricityconsumption.model.energyreport;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for HourConsumptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HourConsumptionType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>float">
 *       &lt;attribute name="ts" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HourConsumptionType", propOrder = {
    "value"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class HourConsumptionType {

    @XmlValue
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected float value;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "dateTime")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected XMLGregorianCalendar ts;

    /**
     * Gets the value of the value property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public float getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * Gets the value of the ts property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public XMLGregorianCalendar getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2018-07-14T04:29:49+02:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setTs(XMLGregorianCalendar value) {
        this.ts = value;
    }

}
