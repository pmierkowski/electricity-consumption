package pl.pmierkowski.electricityconsumption.model;

import pl.pmierkowski.electricityconsumption.exception.InvalidGroupException;

public enum GroupByField {
    MONTH("yyyy-MM"), WEEK("yyyy-MM-W"), DAY("yyyy-MM-dd");

    private String value;

    GroupByField(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static GroupByField fromString(String groupBy) {
        String upperCaseFormat = groupBy.toUpperCase();

        if (isValidFormat(upperCaseFormat)) {
            return GroupByField.valueOf(upperCaseFormat);
        }

        throw new InvalidGroupException(groupBy);
    }

    public static boolean isValidFormat(String format) {
        for (GroupByField value : GroupByField.values()) {
            if (value.toString().equals(format)) {
                return true;
            }
        }

        return false;
    }
}
