package pl.pmierkowski.electricityconsumption.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean("finestmediaUriComponentsBuilder")
    public UriComponentsBuilder uriComponentsBuilder(@Value("${finestmedia.api.rest.url}") String url) {
        return UriComponentsBuilder.fromHttpUrl(url);
    }
}
