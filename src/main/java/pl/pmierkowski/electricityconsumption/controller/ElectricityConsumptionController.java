package pl.pmierkowski.electricityconsumption.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ElectricityConsumptionController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
