package pl.pmierkowski.electricityconsumption.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.pmierkowski.electricityconsumption.model.Consumption;
import pl.pmierkowski.electricityconsumption.model.GroupByField;
import pl.pmierkowski.electricityconsumption.service.ElectricityConsumptionService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
public class ElectricityConsumptionRestController {

    private ElectricityConsumptionService electricityConsumptionService;

    @Autowired
    public ElectricityConsumptionRestController(ElectricityConsumptionService electricityConsumptionService) {
        this.electricityConsumptionService = electricityConsumptionService;
    }

    @RequestMapping(value = "energy-reports", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<Consumption> energyReports(
            @RequestParam(value = "start") @Valid @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate start,
            @RequestParam(value = "end") @Valid @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate end,
            @RequestParam(value = "group_by") String groupBy) {
        return this.electricityConsumptionService.getGroupedConsumption(start, end, GroupByField.fromString(groupBy));
    }
}
