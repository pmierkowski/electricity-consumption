const ENERGY_REPORT_ENDPOINT = "energy-reports";

jQuery(document).ready(function () {
    ElectricityConsumption.createDatePicker();

    ElectricityConsumption.form.on('submit', function (event) {
        event.preventDefault();

        ElectricityConsumption.getData();
    })
});

var ElectricityConsumption = {
    datePicker: $('#datepicker'),
    loader: $('#loader-wrapper'),
    form: $('#search-consumption-form'),
    electricityConsumptionContainer: $('#electricity-consumption-container'),
    errorWrapper: $('#error-wrapper'),
    errorContainer: $('#error-container'),
    lineChart: null,

    createDatePicker() {
        this.datePicker.datepicker({
            format: "dd-mm-yyyy",
            endDate: "-1d",
            maxViewMode: 0,
            autoclose: true,
            todayHighlight: true
        });
    },

    getData: function () {
        this.showLoader();
        this.hideError();

        let that = this;

        $.ajax({
            url: ENERGY_REPORT_ENDPOINT + "?" + this.form.serialize()
        }).done(function (data) {
            that.printHtmlFromData(data);
            that.createChartFromData(data);

            that.hideLoader();
        }).fail(function (jqxhr, textStatus, error) {
            that.showError(jqxhr);

            that.hideLoader();
        });
    },

    printHtmlFromData(data) {
        let price = $('#price').val();
        let htmlResult = '';

        if(price === ""){
            price = 1;
        }

        data.forEach(function (row) {
            htmlResult += '<tr><td>' + row.date + '</td><td>' + parseFloat(row.usage).toFixed(2) + ' kW/h</td><td>' + parseFloat(row.usage * price).toFixed(2) + '</td></tr>';
        });

        this.electricityConsumptionContainer.html(htmlResult);
    },

    createChartFromData(data) {
        let chartLabels = [];
        let chartData = [];

        data.forEach(function (row) {
            chartLabels.push(row.date);
            chartData.push(parseFloat(row.usage).toFixed(2));
        });

        let ctx = document.getElementById("myChart");

        //We could have only one Chart instance
        if (this.lineChart === null) {
            this.lineChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: chartLabels,
                    datasets: [{
                        label: 'Electricity consumption in kW/h',
                        data: chartData,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
        else {
            this.lineChart.data.datasets[0].data = chartData;
            this.lineChart.data.labels = chartLabels;
            this.lineChart.update();
        }
    },

    showLoader() {
        this.loader.show();
    },

    hideLoader() {
        this.loader.hide();
    },

    showError(jqxhr) {
        let err = eval("(" + jqxhr.responseText + ")");

        this.electricityConsumptionContainer.html('');
        this.lineChart.clear();

        this.errorWrapper.show();
        this.errorContainer.html(err.message);
    },

    hideError() {
        this.errorWrapper.hide();
    }
}